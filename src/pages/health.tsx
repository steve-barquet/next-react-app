// ---Dependencys
import { ReactElement } from 'react';
import Head from 'next/head';
// ---Components
import { HealthCont } from 'Cont/HealthCont/HealthCont';
/**
 * Componente HealthPage: este componente es para dar datos al Helmet de
 * la página y concatenarla con el contenedor de la página componente
 * @returns { ReactElement } ReactElement
 */
export default function HealthPage(): ReactElement {
  return (
    <>
      <Head>
        <title>HEALTH</title>
      </Head>
      <HealthCont />
    </>
  );
}
