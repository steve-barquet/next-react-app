// ---Dependencys
import { ReactElement, useEffect } from 'react';
// ---Types
import { AppProps } from 'next/app';
// ---Others
import 'Styles/index.less';
// ---Redux

// ---Components
import { GlobalProvider } from 'GlobalProvider/GlobalProvider';
import { GlobalLayout } from 'GlobalLayout/GlobalLayout';

/** Componente root de nextjs */
function App({ Component, pageProps }: AppProps): ReactElement {
  // --- Const, Hooks, States

  return (
    <GlobalProvider>
      <GlobalLayout>
        <Component {...pageProps} />
      </GlobalLayout>
    </GlobalProvider>
  );
}

export default App;
