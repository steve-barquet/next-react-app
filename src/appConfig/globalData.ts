import { getColors } from './getColors';

export const isProd = process?.env?.NODE_ENV === 'production';

export const isBrowser = typeof window !== 'undefined';

export const healthPass = process?.env?.NEXT_PUBLIC_HEALTH_PASS;

export const appColors = getColors();
