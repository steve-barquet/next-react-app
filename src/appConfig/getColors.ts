import { CSSProperties } from 'react';

export type Colors = {
  primary: NonNullable<CSSProperties['color']>;
  secondary: NonNullable<CSSProperties['color']>;
  [key: string]: CSSProperties['color'];
};

type ColorAsertion = {
  '@colorPrimary': NonNullable<CSSProperties['color']>;
  '@colorSecondary': NonNullable<CSSProperties['color']>;
};

/** Trae los "colors.less" de la app, crashea (y debe crashear) si no cargavlos colores exitosamente */
export function getColors() {
  let colors: Colors = JSON.parse(process.env.NEXT_PUBLIC_LESS_COLORS as string);
  colors = {
    ...colors,
    primary: (colors as unknown as ColorAsertion)['@colorPrimary'],
    secondary: (colors as unknown as ColorAsertion)['@colorSecondary'],
  };
  return colors;
}
