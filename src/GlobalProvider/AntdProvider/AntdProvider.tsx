// ---Dependencys
import React, { ReactElement, ReactNode } from 'react';
import { ConfigProvider, theme } from 'antd';
// ---Config
import { appColors } from 'AppConfig/globalData';

interface Props {
  children: ReactNode;
}
/**
 * AntdProvider component: Proveedor de tema de antd, parra editar gama de colores
 * @returns {ReactNode}
 */
export function AntdProvider({ children }: Props): ReactElement {
  const primary = appColors?.primary || '#2db7f5';
  return (
    <ConfigProvider
      theme={{
        algorithm: theme.defaultAlgorithm,
        token: {
          colorPrimary: primary,
        },
      }}
    >
      {children}
    </ConfigProvider>
  );
}
