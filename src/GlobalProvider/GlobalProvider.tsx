// --- Dependency´s
import { ReactElement, ReactNode } from 'react';
// ---Components
import { AntdProvider } from './AntdProvider/AntdProvider';

// -----------------------PROPS
interface Props {
  children: ReactNode;
}
/**
 * GlobalProvider Component: Componente que se representa globalmente en la aplicación y
 * persistir en todas las páginas. Puede agregar nuevos componentes aquí para representarlos
 * globalmente para Ex: Barras de herramientas, pies de página, componentes de chat, carritos, etc.
 * @param {Props} - Props of the component
 * @return {ReactElement} ReactElement
 */
export function GlobalProvider({ children }: Props): ReactElement {
  // -----------------------RENDER
  return (
    <>
      <AntdProvider>{children}</AntdProvider>
    </>
  );
}
