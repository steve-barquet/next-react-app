// ---Dependencies
import { useRouter } from 'next/router';
// ---Redux
import { useAppInfoStoreV3 } from '@Redux/appInfo/store';
// ---Custom hooks
import { useIsomorphicLayoutEffect } from 'Utils/customHooks/useIsomorphicLayoutEffect';
import { NextParsedUrlQuery } from 'next/dist/server/request-meta';
import { isBrowser } from 'AppConfig/globalData';

/** Hook para escuchar y capturar la ruta actual y los parámetros en la url de la aplicación */
export function useRouteListen(): void {
  const { patch } = useAppInfoStoreV3();
  const router = useRouter();
  const currentPath = router?.route;
  const params: NextParsedUrlQuery = router?.query;

  useIsomorphicLayoutEffect(() => {
    patch({ currentPath });
  }, [currentPath, isBrowser]);
  useIsomorphicLayoutEffect(() => {
    patch({ currentParams: params });
  }, [params]);
}
