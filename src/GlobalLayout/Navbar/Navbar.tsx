// ---Dependencies
import { Button, Col, Row } from 'antd';
import Link from 'next/link';
import { ReactElement } from 'react';
// ---Redux
import { useHealthInfoStore } from '@Redux/healthInfo/store';
// ---Utils
import { responsiveBasicGrid } from 'Utils/functions/responsiveUtils';

const gridProps = responsiveBasicGrid(8);

/**
 * Navbar Component: Ejemplo de navbar para la aplicación
 * @returns {ReactElement} ReactElement
 */
export function Navbar(): ReactElement {
  const { lastPass } = useHealthInfoStore(['lastPass']);
  // -----------------------RENDER
  return (
    <nav className="Navbar">
      <Row>
        <Col {...gridProps}>
          <h3>{lastPass}</h3>
        </Col>
        <Col {...gridProps}>
          <Link href="/" scroll>
            <Button type="link">Home</Button>
          </Link>
        </Col>
        <Col {...gridProps}>
          <Link href="/health">
            <Button type="link">Health</Button>
          </Link>
        </Col>
      </Row>
    </nav>
  );
}
