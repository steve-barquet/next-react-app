// ---Dependencies
import { ReactElement } from 'react';
// ---UI Dependencies
import { Progress } from 'antd';
// ---Config
import { appColors } from 'AppConfig/globalData';

/**
 * ProgressCustom Component: Revisa los colores en redux y personaliza la barra de progreso
 * @returns {ReactElement} ReactElement
 */
export function ProgressCustom(): ReactElement {
  // -----------------------CONSTS, HOOKS, STATES
  const from = appColors?.primary;
  const to = appColors?.secondary;

  // -----------------------RENDER
  return (
    <Progress
      strokeColor={{
        from,
        to,
      }}
      percent={67}
      status="active"
    />
  );
}
