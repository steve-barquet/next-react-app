// ---Dependencys
import React from 'react';
// ---UI dependencys
import { Card, Descriptions } from 'antd';
// ---Config
import {
  commitID,
  commitMssg,
  commitAuthor,
  commitBranch,
  commitDate,
} from 'AppConfig/health/appVersion';
import { envsData } from 'AppConfig/health/envsData';

/**
 * Componente FrontendInfo: Texto que se mostrara en la pagina
 * @returns { ReactElement } ReactElement
 */
export function FrontendInfo(): React.ReactElement {
  return (
    <>
      <Descriptions style={{ width: '100%' }} title="Monitoreo y salud de la App" bordered>
        <Descriptions.Item label="Commit ID">{commitID}</Descriptions.Item>
        <Descriptions.Item label="Commit Message" span={2}>
          {commitMssg}
        </Descriptions.Item>
        <Descriptions.Item label="Commit Author">{commitAuthor}</Descriptions.Item>
        <Descriptions.Item label="Commit Branch">{commitBranch}</Descriptions.Item>
        <Descriptions.Item label="Commit Date">{commitDate}</Descriptions.Item>
        <Descriptions.Item label="NODE_ENV">{process?.env?.NODE_ENV}</Descriptions.Item>
      </Descriptions>
      <Card title="Envs">
        <pre style={{ whiteSpace: 'pre' }}>{JSON.stringify(envsData, null, ' ')}</pre>
      </Card>
    </>
  );
}
