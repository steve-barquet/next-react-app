// ----------------- REDUCER -----------------
export interface HealthInfoInitial {
  verified: boolean;
  lastPass: string;
}
