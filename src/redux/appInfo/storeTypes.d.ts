import { NextParsedUrlQuery } from 'next/dist/server/request-meta';

// ---------------DATA STRUCTURE ---------------
export interface ResponsiveData {
  isMovil: boolean;
  winSize: 'xs' | 'sm' | 'md' | 'lg' | 'xl' | 'xxl';
}
// ----------------- REDUCER -----------------
export interface AppInfoInitial extends ResponsiveData {
  isLoading: boolean;
  currentPath: string;
  currentParams?: NextParsedUrlQuery;
}
