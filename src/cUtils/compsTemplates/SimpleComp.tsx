// ---Dependencies
import { ReactElement } from 'react';
// ---UI Dependencies
// ---Custom Hooks
// ---Redux
// ---Components
// ---Custom Hooks
// ---AppConfig
// ---Assets
// ---Utils
// ---Requests
// ---Images

interface Props {}
/**
 * SimpleComp Component: Do something
 * @param {Props} props - Parametros del componente como: ...
 * @returns {ReactElement}
 */
export function SimpleComp(props: Props): ReactElement {
  // -----------------------CONSTS, HOOKS, STATES
  // -----------------------MAIN METHODS
  // -----------------------AUX METHODS
  // -----------------------RENDER
  return <p>SimpleComp</p>;
}
