// ---Dependencies
import { ReactElement } from 'react';
// ---UI Dependencies
// ---Custom Hooks
// ---Redux
// ---Components
// ---Custom Hooks
// ---AppConfig
// ---Assets
// ---Utils
// ---Requests
// ---Images

interface Props {}
/**
 * useXXXXXX Component: Do something
 * @param {Props} props - Parametros del hook como: ...
 * @returns {ReactElement}
 */
export function useXXXXXX(props: Props): ReactElement {
  // -----------------------CONSTS, HOOKS, STATES
  // -----------------------MAIN METHODS
  // -----------------------AUX METHODS
  // -----------------------RENDER
  // -----------------------HOOK DATA
  return <p>useXXXXXX</p>;
}
