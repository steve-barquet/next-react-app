/**
 * responsiveBasicGrid function: Handle the grid for responsivity basic behaviors for being used in paramas for antd cols
 * @param {number} normalSize - Number to handle the size of antd colums in mid to xxl screens
 * @returns
 */
export function responsiveBasicGrid(normalSize: number) {
  return {
    xs: 24,
    sm: 24,
    md: 24,
    lg: normalSize,
    xl: normalSize,
    xxl: normalSize,
  };
}
