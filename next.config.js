/* eslint-disable */
const withLess = require('next-with-less');
const { env } = require('./env/configs/envsParser');


/**
 * @type {import('next').NextConfig}
 */
module.exports = () => {
  const plugins = [withLess];
  const config = plugins.reduce((acc, next) => next(acc), {
    env,
    output: 'standalone',
  });
  return config;
};