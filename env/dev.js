const { generalDev } = require('./profiles/generalDev');

/**
 * Constante donde debes importar y asignar el perfil de envs que quieres para el ambiente de "Development"
 */
exports.devEnvs = generalDev;
