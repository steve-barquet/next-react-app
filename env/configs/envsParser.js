const { devEnvs, prodEnvs } = require('../envsLoaded');


/**
 * Recibe el objeto con todas las envs y parsea todos los valores a string
 * @returns {{[s: string]: string}} diccionario de strings
 */
function envsParser() {
  const isProd = process.env.NODE_ENV === 'production';
  const envs = isProd?prodEnvs: devEnvs;
  return envs
};

exports.env = envsParser();