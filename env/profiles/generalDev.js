const { getLessVars } = require('../configs/getLessVars');

/**
 * - En la linea 5 está el ejemplo de como leer envs desde la terminal
 * - Todos los nombres deben llevar el prefijo "NEXT_PUBLIC_" para que la app pueda leer las envs
 */
exports.generalDev = {
  /** Nombre del perfil de 'env' */
  NEXT_PUBLIC_PROFILE: 'generalDev',
  /** Puerto donde corre el server de desarrollo */
  NEXT_PUBLIC_PORT: 3000,
  /** URL del backend */
  NEXT_PUBLIC_API_URL: 'http://localhost:4000',
  /** Nombre de la app */
  NEXT_PUBLIC_APP_NAME: 'REACT APP DEVELOPMENT',
  /** Carga automáticamente las variables 'colors' de less como env */
  NEXT_PUBLIC_LESS_COLORS: getLessVars(),
};
