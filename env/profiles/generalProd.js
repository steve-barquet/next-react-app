const { getLessVars } = require('../configs/getLessVars');
/* const { untrackedEnvs } = require('../untracked/env'); */

/**
 * - En la linea 5 está el ejemplo de como leer envs desde la terminal
 * - Todos los nombres deben llevar el prefijo "NEXT_PUBLIC_" para que la app pueda leer las envs
 */
exports.generalProd = {
  /** Password para acceder al health endpoint */
  NEXT_PUBLIC_HEALTH_PASS:
    process.env?.NEXT_PUBLIC_HEALTH_PASS /* || untrackedEnvs?.NEXT_PUBLIC_HEALTH_PASS */,
  /** Nombre del perfil de 'env' */
  NEXT_PUBLIC_PROFILE: 'generalProd',
  /** Puerto donde corre el server de desarrollo */
  NEXT_PUBLIC_PORT: 8080,
  /** URL del backend */
  NEXT_PUBLIC_API_URL: 'http://localhost:4000',
  /** Nombre de la app */
  NEXT_PUBLIC_APP_NAME: 'REACT APP PRODUCTION',
  /** Carga automáticamente las variables 'colors' de less como env */
  NEXT_PUBLIC_LESS_COLORS: getLessVars(),
};
