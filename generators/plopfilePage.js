module.exports = (plop) => {
  plop.setGenerator('component', {
    description: 'Create a component',
    prompts: [
      {
        type: 'input',
        name: 'route',
        message: 'Cual es la ruta de tu "PAGE"?',
      },
      {
        type: 'input',
        name: 'name',
        message: 'Cual es el nombre de tus componentes (sin Prefijos o Sufijos)?',
      },
    ],
    actions: [
      {
        type: 'add',
        path: '../src/pages/{{lowerCase route}}.tsx',
        templateFile: 'templates/page.tsx.hbs',
      },
      {
        type: 'add',
        path: '../src/containers/{{pascalCase name}}Cont/{{pascalCase name}}Cont.tsx',
        templateFile: 'templates/Container.tsx.hbs',
      },
    ],
  });
};
