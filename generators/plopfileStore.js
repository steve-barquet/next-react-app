module.exports = (plop) => {
  plop.setGenerator('component', {
    description: 'Create a component',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'Cual es el nombre de tu Store en camelcase "likeThis"?',
      },
    ],
    actions: [
      {
        type: 'add',
        path: '../src/redux/{{camelCase name}}/store.ts',
        templateFile: 'templates/store.ts.hbs',
      },
      {
        type: 'add',
        path: '../src/redux/{{camelCase name}}/storeTypes.ts',
        templateFile: 'templates/storeTypes.d.ts.hbs',
      },
      {
        type: 'add',
        path: '../src/redux/{{camelCase name}}/actions.ts',
        templateFile: 'templates/actions.ts.hbs',
      },
    ],
  });
};
