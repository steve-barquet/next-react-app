module.exports = (plop) => {
  plop.setGenerator('component', {
    description: 'Create a component',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'Cual es el nombre de tu hook?',
      },
    ],
    actions: [
      {
        type: 'add',
        path: '../src/appConfig/{{pascalCase name}}.ts',
        templateFile: 'templates/Hook.ts.hbs',
      },
    ],
  });
};
