const fs = require('fs');
const path = require('path');
const { devEnvs, prodEnvs } = require('../../../env/envsLoaded');

/** Script that runs when the repo starts as prod or dev */
async function main() {
  const isProd = process?.env?.ENV === 'production';

  const currentEnvs = isProd ? prodEnvs : devEnvs;

  const fullFile = path.join(__dirname, '../../../src/appConfig/health/envsData.ts');

  let file = 'export const envsData= {\n';

  const envsKeys = Object.keys(currentEnvs);
  envsKeys.forEach((key) => {
    file = `${file}\t${key}: ${'`'}${currentEnvs[key]}${'`'},\n`;
  });
  file += '}';

  fs.writeFile(fullFile, file, (err) => {
    if (err) throw err;
    console.log('Archivo de version creado en ->', fullFile);
  });
}

main();
